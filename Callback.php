<?php
/**
 * Class Callback
 *
 * This class contains functionality and helpers to request a call back
 * Also it return all callback slots
 *
 *
 * @package  Callback
 * @author   Sergiu Canschi <sergiu.c@live.com>
 * @version  Version:  1.0
 * @access   MoJ
 * @see      localhost
 */

class Callback {


    protected $datetime;
    protected $name;
    protected $phone;

    const MAX_WORKING_DAYS = 6;
    const MAX_FUTURE_HOURS = 2;
    const WEEK_CLOSE_TIME = '8:00 pm';
    const SATURDAY_CLOSE_TIME = '12:30 pm';
    const OPEN_TIME = '9:00 am';
    const AFTER_WEEKEND_OPEN = '11:00 am';


    /**
     * Function  to check if a given date/time is valid and good to be requested as callback
     * Currently it stops and show a message if dates are invalid
     * The idea is to reuse this function to check other dates
     * Those little check could be separated into mini functions if we might require more repetitive checks like that
     * @param DateTime|null $datetime
     * @return boolean
     */
    public function check(DateTime $datetime = null )
    {
        // initialise datetime object in case it was not passed through
        if ($datetime == null){
            $date  = date("Y-m-d\TH:i:s", strtotime($this->datetime));
            // create a DateTime object from selected date
            $datetime =  new DateTime($date);
        }

        // create a current DateTime object
        $dateTimeNow = new DateTime();

        // check if date selected is not earlier
        if ($datetime < $dateTimeNow){
            die('Not earlier please');
        }

        // check if its not sunday
        $dayOfTheWeek = $datetime->format('N');

        if ( $dayOfTheWeek == 7) {
            die('Not a sunday');
        }

        // fecth bank holidays
        $bankHolidays = $this->fetchBankHolidays(true);
        // check if date selected is not a bank holiday
        if (in_array($datetime->format('Y-m-d'),$bankHolidays)){
            die('Not on a Bank Holiday');
        }


        // fetch working days
        $WorkingDays = $this->getWorkingDays($datetime);
        // check if its within working days
        if ( $WorkingDays > self::MAX_WORKING_DAYS  ){
            die('Not more than 6 working days');
        }


        // check if selected call back datetime is not earlier then 2 hours in case user selected today
        $diff = $dateTimeNow->diff( $datetime );
        $diffHours = (integer)$diff->format( "%h" );

        if ( $diffHours < self::MAX_FUTURE_HOURS  ){
            die('Not earlier then 2 hours');
        }


        // check if time is not out of hours
        $dateSelected = $datetime->format('H:i a');
        $date1 = DateTime::createFromFormat('H:i a', $dateSelected);
        $open = DateTime::createFromFormat('H:i a', self::OPEN_TIME);

        // if its Saturday
        if ( $dayOfTheWeek == 6) {
            $close = DateTime::createFromFormat('H:i a', self::SATURDAY_CLOSE_TIME);
            // if its anything else
        }else{
            $close = DateTime::createFromFormat('H:i a', self::WEEK_CLOSE_TIME);
        }

        if ($date1 < $open ||  $date1 > $close ) {
            die ('In working hours please');
        }


        // get current day of the week
        $dayOfTheWeekNow = $dateTimeNow->format('N');


        // the block below handles the case when its Friday after hours or weekend  and checks
        // that earliest call back is after 11 am

        // if its Saturday
        if ( $dayOfTheWeekNow == 6 || $dayOfTheWeekNow == 5) {


            $dateTimeNowFormated = $dateTimeNow->format('H:i a');
            $date1 = DateTime::createFromFormat('H:i a', $dateTimeNowFormated);

            // check if its Sarturday
            if ($dayOfTheWeekNow == 6){
                $close = DateTime::createFromFormat('H:i a', self::SATURDAY_CLOSE_TIME);
            }else{
                $close = DateTime::createFromFormat('H:i a', self::WEEK_CLOSE_TIME);
            }

            // if current time is more than close time
            if ( $date1 > $close ) {

                $dateTimeNextWeekDay = $dateTimeNow->modify('+1 weekday');

                $dateSelected = DateTime::createFromFormat('H:i a', $dateSelected);

                $open = DateTime::createFromFormat('H:i a', self::AFTER_WEEKEND_OPEN );
                // check if selected date matches next working day
                if ($dateTimeNextWeekDay->format('Y-m-d') == $datetime->format('Y-m-d')) {
                    // check if its not after 11 am
                    if ($dateSelected < $open  ) {
                        die ('Only from 11 on '. $dateTimeNextWeekDay->format('d-m-Y'));
                    }
                }
            }
            // if its Sunday
        }elseif ( $dayOfTheWeekNow == 7 ){

            $dateTimeNextWeekDay = $dateTimeNow->modify('+1 weekday');

            $open = DateTime::createFromFormat('H:i a', self::AFTER_WEEKEND_OPEN );
            // check if selected date matches next working day
            if ($dateTimeNextWeekDay->format('Y-m-d') == $datetime->format('Y-m-d')) {
                // check if its not after 11 am
                if ($date1 < $open  ) {
                    die ('Only from 11 on '. $dateTimeNextWeekDay->format('d-m-Y'));
                }

            }

        }

        return true;


    }

    /**
     * Function to get all callback slots for the next given amount of days
     * In ZF this could be a view helper
     */
    public function getAllCallBackSlots()
    {
        $begin = new DateTime();

        $dateNowFormatted = $begin->format('H:i a');
        $dateNow = DateTime::createFromFormat('H:i a', $dateNowFormatted);

        // check if day is Saturday
        if ( $begin->format('N') == 6) {
            $close = DateTime::createFromFormat('H:i a', self::SATURDAY_CLOSE_TIME);
            // check if day is any other day
        }else{
            $close = DateTime::createFromFormat('H:i a', self::WEEK_CLOSE_TIME);
        }

        if ( $dateNow > $close ) {
            $begin = $begin->modify('+1 day');
        }


        // getting bank holidays
        $bankHolidays = $this->fetchBankHolidays(true);

        $end = new DateTime( );
        $end = $end->modify('+6 days'); // adding 6 days

        for($i = $begin; $i <= $end; $i->modify('+1 day')){


            // check if if date is in bank holidays.
            if (in_array($i->format('Y-m-d'),$bankHolidays)){
                echo  $i->format("Y-m-d").' - ( Bank Holiday )';
            }else{

                $dayOfTheWeek = $i->format('N');

                // check if its Sunday
                if ( $dayOfTheWeek == 7) {
                    echo  $i->format("Y-m-d").' - ( Sunday )';
                }else{

                    $open = DateTime::createFromFormat('H:i a', self::OPEN_TIME);
                    // check if its Saturday
                    if ( $dayOfTheWeek == 6) {
                        echo  $i->format("Y-m-d").' - ( Saturday )';
                        echo "<br>";
                        $close = DateTime::createFromFormat('H:i a', self::SATURDAY_CLOSE_TIME);

                    }else{
                        echo '<strong>'.$i->format("Y-m-d").' - ('. $i->format('l'). ' ) </strong>';
                        echo "<br>";
                        $close = DateTime::createFromFormat('H:i a', self::WEEK_CLOSE_TIME);
                    }

                    $interval = new DateInterval('PT30M');
                    $period   = new DatePeriod($open, $interval, $close);

                    echo '<ul>';
                    foreach ($period as $dt) {

                        if ($dt instanceof DateTime){
                            echo '<li>'.$dt->format("h:ia").'</li>';
                        }

                    }
                    echo '</ul>';

                }

            }
            echo "<br>";


        }


    }

    /**
     * Function that saves the callback data in DB / Session / Sends an email or whatever other action needs to be performed
     */
    public function save()
    {
        echo " All good ! Call back has been registered !";
    }


    /**
     * Helper function which strip the POST string from malicious code.
     * @param $var
     * @return string
     */
    private function cleanPost($var)
    {
        return strip_tags($var);
    }

    /**
     * Function to fetch bank holidays from a specifis API
     * If FilterDates is true it will return full tree of data in case somewhere in the code we need it
     *
     * @param bool $filterDates
     * @return array|mixed
     * @throws Exception
     */
    private function fetchBankHolidays($filterDates = false)
    {
        $endPoint =  'https://www.gov.uk/bank-holidays/england-and-wales.json';

        $json = file_get_contents($endPoint);

        if (!$json){
            throw new Exception('Data could not be fecthed from 3rd party API');
        }

        $holidays =  json_decode($json,true);

        if ($filterDates == true){

            $holidaysDates = [];

            foreach ($holidays['events'] as $holiday){
                $holidaysDates[] =  $holiday['date'];
            }

            return $holidaysDates;
        }

        return $holidays;

    }

    /**
     * Function which fetches working days from now to a given date in the future
     * It excludes  bank holidays, normal holidays and weekends.
     * @param DateTime $to
     * @return int
     */
    private function getWorkingDays(DateTime $to)
    {

        $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
        $holidayDays = ['*-12-25', '*-01-01', '2013-12-23']; # variable and fixed holidays


        $from = new DateTime();
        $interval = new DateInterval('P1D');
        $periods = new DatePeriod($from, $interval, $to);

        $bankHolidays = $this->fetchBankHolidays(true);

        $days = 0;
        foreach ($periods as $period) {
            if (!in_array($period->format('N'), $workingDays)) continue;
            if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
            if (in_array($period->format('*-m-d'), $holidayDays)) continue;
            if (in_array($period->format('Y-m-d'), $bankHolidays)) continue;
            $days++;
        }
        return $days;

    }

    /**
     * @return mixed
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param mixed $datetime
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $this->cleanPost($datetime);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $this->cleanPost($name);
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $this->cleanPost($phone);
    }




}