<?php
//setting the default timezone to be used by date/time functions.
date_default_timezone_set('UTC');

// require Callback php file in order to access class methods
require 'Callback.php';

//instantiate class / create an object
$callback =  new Callback();

// action setters and set populate class with some data
$callback->setDatetime($_POST['datetime']);
$callback->setName($_POST['name']);
$callback->setPhone($_POST['phone']);

// function  to check stuff
$response = $callback->check();

//save the data is check/validation has gone through
if ($response){
    $callback->save();
}






