<?php require "Callback.php";
$dateNow = new DateTime();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Call Center Availability</title>

    <style>
        .form-block {
            margin-bottom: 10px;
        }
    </style>

</head>
<body>

<h1>Call center availability</h1>


    <form id="callback-request" action="callback-process.php" method="post">

        <div class="form-block">
            <label>Phone</label>
            <input required type="text" name="phone" placeholder="Enter your phone ..." value="">
        </div>
        <div class="form-block">
            <label>Date/Time</label>
            <input required min="<?php echo $dateNow->format('Y-m-d') ?>T09:00" type="datetime-local" name="datetime"  step="1800" value="">
        </div>
        <div class="form-block">
            <label>Name</label>
            <input required type="text" name="name" placeholder="Name ..." value="">
        </div>
        <button type="submit" name="submit" >Request call back</button>

    </form>



    <div>
        <h2>All callback half hourly slots for the next 6 days </h2>
        <?php
        // normally this will be pulled from the controller into view
        $callBack = new Callback();
        $callBack->getAllCallBackSlots();
        ?>

    </div>


</body>

</html>